/**
 * Main application file
 */

'use strict';

// Set default node environment to developmente
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');
// Setup server
var router = require('./routes');
var upcomingIpoUpdater = require('./services/UpcomingIpoUpdater');
var symbolDownloadService = require('./services/SymbolDownloadService');

// Setup server
var app = express();

upcomingIpoUpdater.execute();
//symbolDownloadService.parse();

app.use('/rest', router);

app.listen(config.port);

console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));

// Expose app
exports = module.exports = app;