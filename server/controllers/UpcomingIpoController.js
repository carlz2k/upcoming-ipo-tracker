var ipoDao = require('../persistence/UpcomingIpoDao');

function createController() {
    var controller = {};
    controller.getTodayIpos = function (req, res) {
        ipoDao.getTodayIpos(function (ipos, err) {
            if (!err) {
                if (ipos) {
                    res.json(ipos);
                }
            } else {
                console.log("error:" + err);
            }
        });
    };
    return controller;
}

module.exports = createController();