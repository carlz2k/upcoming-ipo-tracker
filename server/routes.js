/**
 * Main application routes
 */

'use strict';
var _ = require('lodash');
var express = require('express');
var errors = require('./components/errors');
var upcomingIpoController = require('./controllers/UpcomingIpoController');

//symbolDownloadService.parse();

function createRouter() {

    var router = express.Router(); 				// get an instance of the express Router
    router.use(function (req, res, next) {
        // do logging
        console.log('Something is happening.');
        next(); // make sure we go to the next routes and don't stop here
    });

    router.get('/todayipos', upcomingIpoController.getTodayIpos);

    router.get('/', function (req, res) {
        res.json({ message: 'hooray! welcome to our api!' });
    });

    router.get('/:url(api|auth|components|app|bower_components|assets)/*',
        function () {
            errors[404];
        });

    return router;
}

module.exports = createRouter();
