var connection = require('./utils/DbUtilsCassandraImpl');
var _ = require('lodash');

function getService() {
    var service = {};
    service.get = function (param_name, value, callbacks) {
        var query = "SELECT * from upcoming_ipo WHERE " + param_name + "=?";
        var params = [value];
        connection.execute(query, params, function (err, result) {
            var ipos = [];
            if (!err && result && result.rows) {
                _.each(result.rows, function (row) {
                    var ipo = convertToUpcomingIpo(row);
                    ipos.push(ipo);
                })
            }

            callbacks(ipos, err);
        });

    };

    service.getTodayIpos = function (callbacks) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        service.get('ipo_date', new Date(mm+'/'+dd+'/'+yyyy) , callbacks);
    };
    service.save = function (object, callbacks) {
        var query = "INSERT INTO upcoming_ipo (symbol, company_name, ipo_date, market, offer_amount," +
            "price_range_high, price_range_low, shares, last_modified_timestamp) " +
            "VALUES (?,?,?,?,?,?,?,?,?)";
        var params = [object.symbol, object.companyName, object.ipoDate,
            object.market, object.offerAmount ? object.offerAmount : 0,
            object.priceRangeHigh ? object.priceRangeHigh : 0,
            object.priceRangeLow ? object.priceRangeLow : 0,
            object.shares ? object.shares : 0,
            object.lastModifiedTimestamp];
        connection.execute(query, params,function (err, result) {
            callbacks(result, err);
        });

    };

    function convertToUpcomingIpo(row) {
        if (row) {
            var ipo = {};
            ipo.companyName = row.company_name;
            ipo.symbol = row.symbol;
            ipo.ipoDate = row.ipo_date;
            ipo.market = row.market;
            ipo.offerAmount = row.offer_amount;
            ipo.priceRangeHigh = row.price_range_high;
            ipo.priceRangeLow = row.price_range_low;
            ipo.shares = row.shares;
            ipo.lastModifiedTimestamp= row.last_modified_timestamp;
            return ipo;
        }
        return undefined;
    }

    return service;
}


module.exports = getService();
