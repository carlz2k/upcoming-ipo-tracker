var connection = require('./utils/DbUtilsCassandraImpl');

function getService() {
    var service = {};

    service.save = function (object, callbacks) {
        var query = "INSERT INTO quote (symbol, market, close, date," +
            "day_high,day_low,dividend_yield,earning_share,open,pe_ratio,volume,year_high,year_low, market_cap, average_volume, type) " +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        var params = [object.symbol, object.market, object.close,
            object.date,
            object.dayHigh,
            object.dayLow,
            object.dividendYield,
            object.earningPerShare,
            object.open,
            object.priceEarningRatio,
            object.volume,
            object.yearHigh,
            object.yearLow,
            object.marketCap,
            object.averageVolume,
        object.type];
        connection.execute(query, params, function (err, result) {
            callbacks(result, err);
        });

    };

    return service;
}


module.exports = getService();
