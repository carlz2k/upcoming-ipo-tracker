var cassandra = require('cassandra-driver');
var client = new cassandra.Client({ contactPoints: ['127.0.0.1'], keyspace: 'market'});

function createService() {
    var service = {};
    service.execute = function(query,params,callbacks){
        client.execute(query, params, {prepare: true}, function (err, result) {
            callbacks(err, result);
        });
    }
    return service;
}
module.exports = createService();


