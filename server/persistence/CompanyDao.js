var connection = require('./utils/DbUtilsCassandraImpl');

function getService() {
    var service = {};

    service.save = function (object, callbacks) {
        var query = "INSERT INTO company (symbol, market, name, market_cap," +
            "ipo_year,sector,industry) " +
            "VALUES (?,?,?,?,?,?,?)";
        var params = [object.symbol, object.market, object.name,
            object.marketCap,
            object.ipoYear,
            object.sector,
            object.industry];
        connection.execute(query, params,function (err, result) {
            callbacks(result, err);
        });

    };

    return service;
}


module.exports = getService();
