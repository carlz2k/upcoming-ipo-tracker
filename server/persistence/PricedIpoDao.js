var connection = require('./utils/DbUtilsCassandraImpl');
var _ = require('lodash');

function getService() {
    var service = {};

    service.getAllPricedIpos = function (callbacks) {
        var query = "SELECT * from priced_ipo";
        connection.execute(query, function (err, result) {
            var ipos = [];
            if (!err && result && result.rows) {
                _.each(result.rows, function (row) {
                    var ipo = convertToIpoObject(row);
                    ipos.push(ipo);
                })
            }
            callbacks(ipos, err);
        });
    };

    service.save = function (object, callbacks) {
        var query = "INSERT INTO priced_ipo (symbol, company_name, ipo_date, market, offer_amount," +
            "starting_price, current_price, shares, last_modified_timestamp) " +
            "VALUES (?,?,?,?,?,?,?,?,?)";
        var params = [object.symbol, object.companyName, object.ipoDate,
            object.market, object.offerAmount ? object.offerAmount : 0,
            object.startingPrice ? object.startingPrice : 0,
            object.currentPrice ? object.currentPrice : 0,
            object.shares ? object.shares : 0,
            object.lastModifiedTimestamp];
        connection.execute(query, params,function (err, result) {
            callbacks(result, err);
        });

    };

    function convertToIpoObject(row) {
        if (row) {
            var ipo = {};
            ipo.companyName = row.company_name;
            ipo.symbol = row.symbol;
            ipo.ipoDate = row.ipo_date;
            ipo.market = row.market;
            ipo.offerAmount = row.offer_amount;
            ipo.shares = row.shares;
            ipo.currentPrice = row.current_price;
            ipo.startingPrice = row.starting_price;
            ipo.lastModifiedTimestamp= row.last_modified_timestamp;
            return ipo;
        }
        return undefined;
    }

    return service;
}


module.exports = getService();
