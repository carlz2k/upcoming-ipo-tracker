var yqlUtil = require("../utils/YqlUtils");
var _ = require('lodash');
var S = require("string");

function createService() {
    var service = {};
    var YAHOO_STOCK_QUOTE_YQL = "select * from yahoo.finance.quotes where symbol in ('{{symbol}}')";

    service.getEodQuote = function (symbol, callbacks) {
        var values = {symbol: symbol};
        var yqlSource = S(YAHOO_STOCK_QUOTE_YQL).template(values).s;
        yqlUtil.executeQuery(yqlSource, function (error, response) {
            if (response.query) {
                var resultResponse = response.query.results;

                if (resultResponse) {
                    var quoteResponse = resultResponse.quote;
                    if (quoteResponse && quoteResponse.Change) {
                        callbacks(quoteResponse);
                        return;
                    }
                }
            }
            callbacks();
        });
    };

    return service;
}

module.exports = createService();
