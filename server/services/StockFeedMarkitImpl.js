var httpRequest = require("../utils/HttpRequestUtils");
var _ = require('lodash');
var BATS = "BATS Trading Inc";

function createService() {
    var service = {};
    service.lookupSymbol = function (symbol, market, callback) {
        httpRequest.get("http://dev.markitondemand.com/Api/v2/Lookup/json?input=" + symbol, {},
            function (data) {
                if (data.length == 1) {
                    callback(data[0]);
                } else if (data.length > 1) {
                    var stock = _.find(data, function (item) {
                        return item.Exchange.toUpperCase() === market.toUpperCase() && item.Symbol.toUpperCase()===symbol.toUpperCase();
                    });

                    if(!stock) {
                        var stock = _.find(data, function (item) {
                            return item.Exchange.toUpperCase() === BATS.toUpperCase() && item.Symbol.toUpperCase()===symbol.toUpperCase();
                        });
                    }
                    callback(stock);
                } else {
                    callback();
                }
            }, function (error) {
                console.log("error is " + error);
            });
    };

    return service;
}

module.exports = createService();
