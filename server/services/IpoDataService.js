var yqlUtil = require("../utils/YqlUtils");
var _ = require('lodash');

function createService() {
    var services = {};



    var UPCOMING_IPO_URL_SOURCE_NASDAQ = yqlUtil.getHtmlContentYqlQuery("http://www.nasdaq.com/markets/ipos/activity.aspx?tab=upcoming");
    var PRICED_IPO_URL_SOURCE_NASDAQ = yqlUtil.getHtmlContentYqlQuery("http://www.nasdaq.com/markets/ipos/activity.aspx?tab=pricings");

    function getIPOInfoFields(sourceBaseUrl, callbacks){
        var yqlSource = sourceBaseUrl + "and xpath='//table/thead/tr/th'";
        var fields = [];
        yqlUtil.executeQuery(yqlSource, function (error, response) {
            if (response && response.query && response.query.results) {
                var headerList = response.query.results.th;
                _.each(headerList, function (item) {
                    fields.push(item.p);
                });
                callbacks(fields);
            } else {
                console.log("error when find any priced ipos");
            }

        });
    }

    function getIPOInfos(sourceBaseUrl, callbacks) {
        var yqlSource = sourceBaseUrl
            + "and xpath='//table/tbody/tr'";

        var infos = [];
        yqlUtil.executeQuery(yqlSource, function (error, response) {
            var infoList = response.query.results.tr;

            _.each(infoList, function (item) {
                var info = [];
                item = item && item.td ? item.td : item;
                _.each(item, function (field) {
                    var value;
                    if (field.a) {
                        value = field.a.content;
                    } else if (field.p) {
                        value = field.p;
                    }
                    info.push(value);
                });
                infos.push(info);
            });
            callbacks(infos);
        });
    }
    services.getPricedIPOInfoFields = function (callbacks) {
        getIPOInfoFields(PRICED_IPO_URL_SOURCE_NASDAQ, callbacks);
    };

    services.getPricedIPOInfos = function(callbacks){
        getIPOInfos(PRICED_IPO_URL_SOURCE_NASDAQ, callbacks)
    };

    services.getUpcomingIPOInfoFields = function (callbacks) {
        getIPOInfoFields(UPCOMING_IPO_URL_SOURCE_NASDAQ, callbacks);
    };

    services.getUpcomingIPOInfos = function(callbacks){
        getIPOInfos(UPCOMING_IPO_URL_SOURCE_NASDAQ, callbacks)
    };

    return services;
}

module.exports = createService();

