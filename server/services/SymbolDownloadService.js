var http = require('http');
var parse = require('csv-parse');
var Company = require('../models/Company');
var Quote = require('../models/Quote');
var stockFeedYahooImpl = require('./StockFeedYahooImpl');
var pauseable = require('pauseable');
var companyDao = require('../persistence/CompanyDao');
var quoteDao = require('../persistence/QuoteDao');

function createService() {
    var service = {};
    service.parse = function () {


        function createNewParser(market) {
            var header = [];
            var headerRead = false;
            var parser = parse({delimiter: ','});
            parser.on('readable', function () {
                var row = parser.read();
                if (!headerRead) {
                    header = row;
                    headerRead = true;
                } else {

                    if (row.length !== header.length) {
                        console.log("error:" + row);
                    }

                    else {
                        var company = new Company(header, row, market);
                        pauseable.setTimeout(
                            function() {
                                stockFeedYahooImpl.getEodQuote(company.symbol, function (quoteResponse) {
                                    if (quoteResponse) {
                                        console.log("symbol found:" + quoteResponse.Name+ " "+quoteResponse.LastTradePriceOnly);
                                        companyDao.save(company, function(result, err){
                                            if(!err) {
                                                console.log(company.symbol+"company saved.");
                                            } else {
                                                console.log("cannot save "+company.symbol);
                                            }
                                        });
                                        var quote = new Quote(quoteResponse, company.market);
                                        if(quote.earningPerShare>0){
                                            quoteDao.save(quote, function(result,err){
                                                if(!err) {
                                                    console.log(quote.symbol+"quote saved.");
                                                } else {
                                                    console.log("cannot save "+quote.symbol);
                                                }
                                            });
                                        }

                                    } else {
                                        console.log("cannot find symbol: " + company.symbol + " " + company.market);
                                    }
                                }, function () {
                                    console.log("symbol lookup:" + company.symbol);
                                })
                            }, 1000);

                    }

                }
            });

            return parser;
        }

        function downloadSymbols(market){
            var url = "http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange="+market+"&render=download";
            http.get(url,
                function (response) {
                    response.pipe(createNewParser(market));
                });
        }

        downloadSymbols("nyse");
        downloadSymbols("nasdaq");
        downloadSymbols("amex");


    };

    return service;
}

module.exports = createService();

