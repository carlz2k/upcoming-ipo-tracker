'use strict';
var _ = require('lodash');
var upcomingIpoDao = require('../persistence/UpcomingIpoDao');
var pricedIpoDao = require('../persistence/PricedIpoDao');
var yqlServices = require('./IpoDataService');
var stockFeedYahooImpl = require('./StockFeedYahooImpl');
var pauseable = require('pauseable');
var Utils = require('../utils/Utils');
var async = require('async');

function createUpdater() {
    var service = {};

    function convertToUpcomingIpo(propertyNameList, ipoValue) {
        var ipo = {};
        ipo.lastModifiedTimestamp = new Date();
        for (var i = 0; i < propertyNameList.length; i++) {
            var propertyName = propertyNameList[i];
            var value = ipoValue[i];
            if (propertyName === 'Symbol') {
                ipo.symbol = value;
            } else if (propertyName === 'Company Name') {
                ipo.companyName = value;
            } else if (propertyName === 'Price') {
                var separator = '-';
                if (value.indexOf(separator) !== -1) {
                    var prices = value.split(separator);
                    ipo.priceRangeLow = parseFloat(prices[0]);
                    ipo.priceRangeHigh = parseFloat(prices[1]);
                } else {
                    value = value.replace(/,/g, '');
                    value = value.replace('$', '');
                    ipo.priceRangeLow = parseFloat(value);
                    ipo.priceRangeHigh = parseFloat(value);
                }
            } else if (propertyName === 'Market') {
                ipo.market = value;
            } else if (propertyName === 'Shares') {
                ipo.shares = parseInt(value.replace(/,/g, ''));
            } else if (propertyName === 'Offer Amount') {
                value = value.replace(/,/g, '');
                value = value.replace('$', '');
                ipo.offerAmount = parseInt(value);
            } else if (propertyName === 'Expected IPO Date') {
                ipo.ipoDate = new Date(value);
            }
        }
        return ipo;
    }

    function convertToPricedIpo(propertyNameList, ipoValue) {
        var ipo = {};
        ipo.lastModifiedTimestamp = new Date();
        if(propertyNameList.length===ipoValue.length){
            for (var i = 0; i < propertyNameList.length; i++) {
                var propertyName = propertyNameList[i];
                var value = ipoValue[i];
                if (propertyName === 'Symbol') {
                    ipo.symbol = value;
                } else if (propertyName === 'Company Name') {
                    ipo.companyName = value;
                } else if (propertyName === 'Price') {
                    value = value.replace(/,/g, '');
                    value = value.replace('$', '');
                    ipo.startingPrice = parseFloat(value);
                } else if (propertyName === 'Market') {
                    ipo.market = value;
                } else if (propertyName === 'Shares') {
                    ipo.shares = parseInt(value.replace(/,/g, ''));
                } else if (propertyName === 'Offer Amount') {
                    value = value.replace(/,/g, '');
                    value = value.replace('$', '');
                    ipo.offerAmount = parseInt(value);
                } else if (propertyName === 'Date Priced') {
                    ipo.ipoDate = new Date(value);
                }
            }
        }
        return ipo;
    }

    function updateIpoQuote(ipo) {
        var symbol = ipo.symbol;
        if(symbol){
            stockFeedYahooImpl.getEodQuote(symbol, function (quoteResponse) {

                if (quoteResponse) {
                    ipo.currentPrice = Utils.toNumeric(quoteResponse.LastTradePriceOnly);
                    pricedIpoDao.save(ipo, function (result, err) {
                        if (!err) {
                            console.log(symbol + "quote saved.");
                        } else {
                            console.log("cannot save " + symbol);
                        }
                    });
                }

            });
        }

    }

    function updateUpcomingIpos() {
        yqlServices.getUpcomingIPOInfoFields(function (propertyNameList) {
            yqlServices.getUpcomingIPOInfos(function (ipoList) {
                _.each(ipoList, function (ipoValue) {
                    var ipo = convertToUpcomingIpo(propertyNameList, ipoValue);

                    upcomingIpoDao.save(ipo, function (result, err) {
                        if (!err) {

                        } else {
                            console.log("error:" + err);
                        }
                    });
                });
            });
        });
    }

    function updatePricedIpos() {
        async.waterfall([
            function (callback) {
                yqlServices.getPricedIPOInfoFields(function (propertyNameList) {
                    callback(null, propertyNameList);
                })
            },
            function (propertyNameList) {
                yqlServices.getPricedIPOInfos(function (ipoList) {
                    _.each(ipoList, function (ipoValue) {
                        var ipo = convertToPricedIpo(propertyNameList, ipoValue);
                        if(ipo){
                            updateIpoQuote(ipo);
                        }

                    });
                });
            }
        ], function (error, result) {
            if (error) {
                console.log("error: " + error);
            }
            if (result) {
                console.log("result = " + result);
            }
        });

    }

    service.execute = function () {

        updatePricedIpos();
        updateUpcomingIpos();
        pauseable.setInterval(10 * 3600 * 1000, function () {
            //updateUpcomingIpos();
            //updatePricedIpos();
        });
    };

    return service;
}

module.exports = createUpdater();

