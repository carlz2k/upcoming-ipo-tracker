var request= require('request');
function createService() {
    var service = {};
    service.get = function(url,data,successCallback, errorCallback) {
        request.get({url:url, body:data, json:true},
            function (error, response, body){

            if(!error && body && response.statusCode == 200){
                successCallback(body, response);
            } else {
                errorCallback(error, response);
            }
        });
    }
    return service;
};

module.exports = createService();
